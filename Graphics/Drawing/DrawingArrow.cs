﻿using System;
using System.Windows;
using System.Windows.Controls;
using Diplom.Logic.Drawing.DrawingOption;

namespace Diplom.Logic.Drawing
{
    public class DrawingArrow : Drawing
    {
        private Vector _modif;
        private Vector _modifOne;

        public DrawingArrow()
        {
            _modif = new Vector();
            _modifOne = new Vector();
        }

        private void DrawSplitter(ref Canvas c, Vector startPoint, DrawingArrowOption option)
        {

            Vector srot = Rotate(_modifOne * 7, DegreeToRadian(90));
            Vector basePoint = startPoint + _modif / 2;

            DrawLine(
                ref c,
                basePoint + (_modifOne * 3) - srot,
                basePoint + (_modifOne * 3) + srot,
                option.ColorThikness, option.SizeThikness);

            DrawLine(
                ref c,
                basePoint - (_modifOne * 3) + srot,
                basePoint - (_modifOne * 3) - srot,
                option.ColorThikness, option.SizeThikness);
        }

        private void DrawArrow(ref Canvas c, Vector startPoint, DrawingArrowOption option)
        {
            Vector srotRightPart = Rotate(_modifOne * 7, DegreeToRadian(180 - 15));
            Vector srotLeftPart = Rotate(_modifOne * 7, DegreeToRadian(180 + 15));

            Vector basePoint = startPoint;

            DrawLine(
                ref c,
                basePoint,
                basePoint + srotRightPart,
                option.ColorThikness, option.SizeThikness);

            DrawLine(
                ref c,
                basePoint,
                basePoint + srotLeftPart,
                option.ColorThikness, option.SizeThikness);
        }

        private void DrawText(ref Canvas c, Vector startPoint, String text,
            DrawingArrowOption option)
        {
            var f = RadianToDegree(Angle(_modif / 2, new Vector(1, 0)));
            var sizeText = new Size(100, 20);
            Vector basePoint = startPoint + _modif / 2;
            Vector offseVector = new Vector(10, 0);

            if (f >= -90 && f <= -90)
            {
                if (option.Side == DrawingEnumArrowSide.Right)
                    DrawText(c, basePoint + offseVector - new Vector(0, sizeText.Height / 2), text, 9, sizeText);
                else
                    DrawText(c, basePoint - offseVector - new Vector(sizeText.Width, sizeText.Height / 2), text, 9, sizeText,TextAlignment.Right);
            }
            else if (f < 0)
            {
                DrawText(c, basePoint + offseVector - new Vector(0, sizeText.Height / 2), text, 9, sizeText);
            }
            else if (f > 0)
            {
                DrawText(c, basePoint - offseVector - new Vector(sizeText.Width, sizeText.Height / 2), text, 9, sizeText,TextAlignment.Right);
            }
        }

        public void DrawArrow(ref Canvas c, Vector startPoint, Vector endPoint, String text,
            DrawingArrowOption option)
        {

            _modif = (endPoint - startPoint);
            _modifOne = _modif / Math.Sqrt(Math.Pow(_modif.X, 2) + Math.Pow(_modif.Y, 2));

            if (option.DrawSplitter)
            {
                DrawLine(ref c, startPoint, startPoint + _modif / 2 - (_modifOne* 3), option.ColorThikness,
                    option.SizeThikness);
                DrawLine(ref c, startPoint + _modif / 2 + (_modifOne * 3), startPoint + _modif, option.ColorThikness,
                    option.SizeThikness);

                DrawSplitter(ref c, startPoint, option);
            }
            else
            {
                DrawLine(ref c, startPoint, endPoint, option.ColorThikness, option.SizeThikness);
            }

            if (option.DrawArrow)
            {
                DrawArrow(ref c, endPoint, option);
            }

            if (option.DrawText)
            {
                DrawText(ref c, startPoint, text, option);
            }
        }
    }
}