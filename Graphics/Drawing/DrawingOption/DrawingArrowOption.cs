﻿using System.Windows.Media;

namespace Diplom.Logic.Drawing.DrawingOption
{
    public class DrawingArrowOption
    {
        public bool DrawSplitter { get; set; } = false;
        public bool DrawArrow { get; set; } = true;
        public bool DrawText { get; set; } = true;
        public DrawingEnumArrowSide Side { get; set; } = DrawingEnumArrowSide.Right;

        public Brush ColorThikness { get; set; } = Brushes.Black;
        public int SizeThikness { get; set; } = 2;
    }
}