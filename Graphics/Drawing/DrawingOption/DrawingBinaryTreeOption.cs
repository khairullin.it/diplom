﻿using System.Windows;

namespace Diplom.Logic.Drawing.DrawingOption
{
    public class DrawingBinaryTreeOption
    {
        public Vector NodeSize { get; set; } = new Vector(50, 50);
        public double VerticalMargin { get; set; } = 20;
        public double HorizontalMargin { get; set; } = 5;

        public DrawingNodeOption NodeOption { get; set; } = new DrawingNodeOption();
        public DrawingArrowOption ArrowOption { get; set; } = new DrawingArrowOption();
    }
}