﻿using System.Windows;
using System.Windows.Media;

namespace Diplom.Logic.Drawing.DrawingOption
{
    public class DrawingNodeOption
    {
        public bool ShowAddress { get; set; } = true;
        public Vector Size { get; set; } = new Vector(50, 50);
        public Brush BorderBrush { get; set; } = new SolidColorBrush(Colors.Black);
        public Brush BackgroundBrush { get; set; } = new SolidColorBrush(Colors.White);
        public Brush ForegroundBrush { get; set; } = new SolidColorBrush(Colors.Black);
        public double FontSize { get; set; } = 14;
        public int BorderThickness { get; set; } = 2;
        public int BorderRadius { get; set; } = 10;
    }
}