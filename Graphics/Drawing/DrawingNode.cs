﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;
using Diplom.Logic.Drawing.DrawingOption;

namespace Diplom.Logic.Drawing
{
    public class DrawingNode : Drawing
    {
        public string GeneralText { get; set; } = "";
        public string LeftChildText { get; set; } = null;
        public string RightChildText { get; set; } = null;

        public UIElement DrawAdressNode(string childNode, DrawingNodeOption option)
        {
            if (childNode == null)
            {
                return new Line
                {
                    Stroke = option.ForegroundBrush,
                    StrokeThickness = 1,
                    X1 = 5,
                    Y1 = (option.Size.Y / 2) - 5,
                    X2 = (option.Size.X / 2) - 5,
                    Y2 = 5
                };
            }
            else
            {
                return new TextBlock
                {
                    Text = string.Format("{0}*",childNode),
                    FontSize = option.FontSize-3,
                    Foreground = option.ForegroundBrush,
                    TextAlignment = TextAlignment.Center,
                    VerticalAlignment = VerticalAlignment.Center,
                    Background = new SolidColorBrush(Colors.Transparent),
                };
            }
        }
        public UIElement GetDrawingNode(Vector posVector, DrawingNodeOption option)
        {
            Border generalNode = new Border
            {
                BorderBrush = option.BorderBrush,
                Background = new SolidColorBrush(Colors.Transparent),
                Child = new TextBlock
                {
                    Text = GeneralText,
                    FontSize = option.FontSize,
                    TextAlignment = TextAlignment.Center,
                    VerticalAlignment = VerticalAlignment.Center,
                    Background = new SolidColorBrush(Colors.Transparent),
                    Foreground = option.ForegroundBrush
                }
            };
            Grid.SetRow(generalNode, 0);

            Grid rowGrid = new Grid();
            rowGrid.RowDefinitions.Add(new RowDefinition {Height = new GridLength(1, GridUnitType.Star)});
            rowGrid.Children.Add(generalNode);

            Border border = new Border
            {
                Width = option.Size.X,
                Height = option.Size.Y,
                BorderBrush = option.BorderBrush,
                Background = option.BackgroundBrush,
                BorderThickness = new Thickness(option.BorderThickness),
                Child = rowGrid,
                CornerRadius = new CornerRadius(option.BorderRadius)
            };

            if (option.ShowAddress)
            {
                generalNode.BorderThickness = new Thickness(0, 0, 0, option.BorderThickness);
                rowGrid.RowDefinitions.Add(new RowDefinition {Height = new GridLength(1, GridUnitType.Star)});

                UIElement leftAddresNode = DrawAdressNode(LeftChildText, option);
                UIElement rightAddresNode = DrawAdressNode(RightChildText, option);
                Border borderAddres = new Border
                {
                    BorderBrush = option.BorderBrush,
                    BorderThickness = new Thickness(option.BorderThickness),
                    Background = new SolidColorBrush(Colors.Transparent),
                };


                Grid colGrid = new Grid();
                colGrid.ColumnDefinitions.Add(new ColumnDefinition {Width = new GridLength(100, GridUnitType.Star)});
                colGrid.ColumnDefinitions.Add(new ColumnDefinition {Width = new GridLength(option.BorderThickness)});
                colGrid.ColumnDefinitions.Add(new ColumnDefinition {Width = new GridLength(100, GridUnitType.Star)});

                colGrid.Children.Add(rightAddresNode);
                colGrid.Children.Add(leftAddresNode);
                colGrid.Children.Add(borderAddres);

                rowGrid.Children.Add(colGrid);

                Grid.SetRow(colGrid, 1);

                Grid.SetColumn(leftAddresNode, 0);
                Grid.SetColumn(borderAddres, 1);
                Grid.SetColumn(rightAddresNode, 2);
            }

            Canvas.SetLeft(border, posVector.X);
            Canvas.SetTop(border, posVector.Y);
            return border;
        }
    }
}