﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace Diplom.Logic.Drawing
{
    public class Drawing
    {
        public static double DegreeToRadian(double angle)
        {
            return Math.PI * angle / 180.0;
        }

        public static double RadianToDegree(double angle)
        {
            return angle * (180.0 / Math.PI);
        }

        public static Vector Rotate(Vector v, double angle)
        {
            return new Vector(
                v.X * Math.Cos(angle) - v.Y * Math.Sin(angle),
                v.X * Math.Sin(angle) + v.Y * Math.Cos(angle)
            );
        }

        /// <summary>
        /// Косинус угла между векторами
        /// </summary>
        /// <param name="A"></param>
        /// <param name="B"></param>
        /// <returns>Angle cos</returns>
        public static double Angle(Vector A, Vector B)
        {
            return Math.Atan((A.X * B.Y - A.Y * B.X) / (A.X * B.X + A.Y * B.Y));
        }

        public static void DrawText(Canvas c, Vector pos, String text, double fontSize, Size box, TextAlignment ta = TextAlignment.Left, VerticalAlignment va =VerticalAlignment.Center)
        {
            TextBlock tb = new TextBlock
            {
                Text = text,
                FontSize = fontSize,
                TextAlignment = ta,
                VerticalAlignment = va,
                Height = box.Height,
                Width = box.Width,
                MinHeight = box.Height,
                MaxHeight = box.Height,
                Padding = new Thickness(2)
            };

            Canvas.SetLeft(tb,pos.X);
            Canvas.SetTop(tb,pos.Y);
            c.Children.Add(tb);
        }

        public static void DrawLine(ref Canvas c, Vector start, Vector end, Brush brush, int thickness = 1)
        {
            Line l = new Line
            {
                X1 = start.X,
                Y1 = start.Y,
                X2 = end.X,
                Y2 = end.Y,
                Stroke = brush,
                StrokeThickness = thickness
            };

            c.Children.Add(l);
        }
    }
}