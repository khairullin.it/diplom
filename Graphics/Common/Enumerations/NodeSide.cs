﻿using System.ComponentModel;

namespace Diplom.Logic.Common.Enumerations
{
    public enum NodeSide
    {
        [Description("Левая")]
        Left,
        [Description("Правая")]
        Right,
        [Description("Корень")]
        Root,
        None
    }
}