﻿using Diplom.Logic.Attributes;

namespace Diplom.Logic.Common.Enumerations
{
    public enum NodeStatus
    {
        [NodeStatus("#8d5a23", "#f1ca8e", "#8d5a23")]
        StepFindNode, // В узле ничего не найдено, узел пропущен

        [NodeStatus("#052e44", "#c0dff1", "#052e44")]
        FindNode, // В узле происходит поиск

        [NodeStatus("#6fb327", "#c4ff85", "#5f9922")]
        FindedNode, // Поиск дал результат

        [NodeStatus("#ff0000", "#000000", "#ff0000")]
        InsertedNode, // Узел который добавлен

        [NodeStatus("#00ff00", "#000000", "#00ff00")]
        PreDeletedNode, // Узел который нужно удалить

        [NodeStatus] None // Узел не участвовал
    }
}