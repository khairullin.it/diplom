﻿using System.Collections.Generic;
using System.Threading;
using System.Windows.Controls;
using Diplom.Logic.Common.Enumerations;

namespace Diplom.Logic.Common
{
    /// <summary>
    /// Бинарное дерево поиска
    /// </summary>
    public class Tree
    {
        public Node Root { get; private set; }
        public ManualResetEvent StepEvent { get; set; } = new ManualResetEvent(false);
        private Thread ThreadFindProp { get; set; }
        private List<HistoryData> History { get; set; } = new List<HistoryData>();

        #region Методы

        /// <summary>
        /// Вычисляет колличество элементов в дереве
        /// </summary>
        /// <returns>Колличество узлов</returns>
        public int Count()
        {
            if (Root != null) return 0;

            return Count(Root);
        }

        /// <summary>
        /// Вставляет узел с заданным ключом в дерево
        /// </summary>
        /// <param name="data">Ключ</param>
        public void Insert(int data)
        {
            ClearStatusAllNode();
            History.Clear();
            SaveHistory($"Проверяем существует, ли у дерева корень.");

            if (Root == null)
            {
                Root = new Node(data);
                Root.NodeStatus = NodeStatus.InsertedNode;
                SaveHistory($"Корень не существует, создаем узел со значением Key({data}), устанавливаем узел как корень.");
            }
            else
            {
                Root.NodeStatus = NodeStatus.FindNode;
                SaveHistory($"Корень существует, спускаемся в корень");
                Insert(data, Root, null);
            }
        }

        /// <summary>
        /// Вычисляет глубину дерева
        /// </summary>
        /// <returns>Глубина дерева</returns>
        public int GetDepth()
        {
            return GetDepth(Root);
        }

        /// <summary>
        /// Удаляет Узел из дерева
        /// </summary>
        /// <param name="removeNode">Удаляемый узел</param>
        public void Remove(Node removeNode)
        {
            ClearStatusAllNode();
            History.Clear();
            if (removeNode == null) return;
            var nodeSide = removeNode.GetSide();

            removeNode.NodeStatus = NodeStatus.PreDeletedNode;
            SaveHistory("");

            //Если у узла нет дочерних элементов, его можно смело удалять
            if (removeNode.LeftChild == null && removeNode.RightChild == null)
            {

                switch (nodeSide)
                {
                    case NodeSide.Left:
                    {
                        removeNode.Parent.NodeStatus = NodeStatus.FindedNode;
                        SaveHistory("");

                        removeNode.Parent.LeftChild = null;
                        SaveHistory("");

                        return;
                    }
                    case NodeSide.Right:
                        removeNode.Parent.NodeStatus = NodeStatus.FindedNode;
                        SaveHistory("");

                        removeNode.Parent.RightChild = null;
                        SaveHistory("");
                        return;
                    case NodeSide.Root:
                        SaveHistory("");
                        SaveHistory("");
                        Root = null;
                        return;
                }
            }

            //Если нет левого дочернего, то правый дочерний становится на место удаляемого
            if (removeNode.RightChild != null)
            {
                removeNode.RightChild.NodeStatus = NodeStatus.FindedNode;
                SaveHistory("");

                switch (nodeSide)
                {
                    case NodeSide.Left:
                        removeNode.Parent.NodeStatus = NodeStatus.FindedNode;
                        SaveHistory("");
                        removeNode.Parent.NodeStatus = NodeStatus.StepFindNode;

                        removeNode.Parent.LeftChild = removeNode.RightChild;
                        removeNode.Parent.LeftChild.NodeStatus = NodeStatus.FindedNode;
                        SaveHistory("");

                        break;
                    case NodeSide.Right:
                        removeNode.Parent.NodeStatus = NodeStatus.FindedNode;
                        SaveHistory("");
                        removeNode.Parent.NodeStatus = NodeStatus.StepFindNode;

                        removeNode.Parent.RightChild = removeNode.RightChild;
                        removeNode.Parent.LeftChild.NodeStatus = NodeStatus.FindedNode;
                        SaveHistory("");
                        break;
                }

                removeNode.RightChild.Parent = removeNode.Parent;

                return;
            }

            //Если нет правого дочернего, то левый дочерний становится на место удаляемого
            if (removeNode.LeftChild != null)
            {
                removeNode.LeftChild.NodeStatus = NodeStatus.FindedNode;
                SaveHistory("");

                switch (nodeSide)
                {
                    case NodeSide.Left:
                        removeNode.Parent.NodeStatus = NodeStatus.FindedNode;
                        SaveHistory("");
                        removeNode.Parent.NodeStatus = NodeStatus.StepFindNode;

                        removeNode.Parent.LeftChild = removeNode.LeftChild;

                        removeNode.Parent.LeftChild.NodeStatus = NodeStatus.FindedNode;
                        SaveHistory("");
                        break;
                    case NodeSide.Right:
                        removeNode.Parent.NodeStatus = NodeStatus.FindedNode;
                        SaveHistory("");
                        removeNode.Parent.NodeStatus = NodeStatus.StepFindNode;

                        removeNode.Parent.RightChild = removeNode.LeftChild;

                        removeNode.Parent.LeftChild.NodeStatus = NodeStatus.FindedNode;
                        SaveHistory("");
                        break;
                }

                removeNode.LeftChild.Parent = removeNode.Parent;
                return;
            }

            //Если присутствуют оба дочерних узла
            //то правый ставим на место удаляемого
            //а левый вставляем в правый

            removeNode.Parent.RightChild.NodeStatus = NodeStatus.FindedNode;
            removeNode.Parent.LeftChild.NodeStatus = NodeStatus.FindedNode;
            SaveHistory("");
            removeNode.Parent.RightChild.NodeStatus = NodeStatus.None;
            removeNode.Parent.LeftChild.NodeStatus = NodeStatus.None;

            if (nodeSide == NodeSide.Left)
            {
                removeNode.Parent.LeftChild.NodeStatus = NodeStatus.FindedNode;
                SaveHistory("");

                removeNode.Parent.LeftChild.NodeStatus = NodeStatus.None;
                removeNode.RightChild.NodeStatus = NodeStatus.FindedNode;
                SaveHistory("");

                removeNode.Parent.LeftChild = removeNode.RightChild;
                SaveHistory("");
            }

            if (nodeSide == NodeSide.Right)
            {
                removeNode.Parent.RightChild.NodeStatus = NodeStatus.FindedNode;
                SaveHistory("");

                removeNode.Parent.RightChild.NodeStatus = NodeStatus.None;
                removeNode.RightChild.NodeStatus = NodeStatus.FindedNode;
                SaveHistory("");

                removeNode.Parent.RightChild = removeNode.RightChild;
                SaveHistory("");
            }

            if (nodeSide == NodeSide.Root)
            {
                SaveHistory("Удаляемый узел это корень");

                var tempLeftChild = removeNode.LeftChild;
                var tempRightLeftChild = removeNode.RightChild.LeftChild;
                var tempRightRightChild = removeNode.RightChild.RightChild;

                tempLeftChild.NodeStatus = NodeStatus.InsertedNode;
                tempRightLeftChild.NodeStatus = NodeStatus.FindNode;
                tempRightRightChild.NodeStatus = NodeStatus.FindNode;
                SaveHistory("");

                removeNode.Data = removeNode.RightChild.Data;
                SaveHistory("");

                removeNode.RightChild = tempRightRightChild;
                SaveHistory("");

                removeNode.LeftChild = tempRightLeftChild;
                SaveHistory("");

                Insert(tempLeftChild, removeNode, removeNode);
                SaveHistory("");
            }
            else
            {
                removeNode.Parent.NodeStatus = NodeStatus.StepFindNode;
                removeNode.RightChild.NodeStatus = NodeStatus.StepFindNode;
                SaveHistory("");
                removeNode.RightChild.Parent = removeNode.Parent;
                Insert(removeNode.LeftChild, removeNode.RightChild, removeNode.RightChild);
                SaveHistory("");
            }
        }

        /// <summary>
        /// Находит узел с заданным ключом
        /// </summary>
        /// <param name="data">Ключ</param>
        /// <returns>Узел</returns>
        public Node Find(int data)
        {
            History = new List<HistoryData>();
            if (Root == null) return null;
            ClearStatusAllNode();

            Node node = ThreadFind(data, Root);

            if (node == null)
            {
                Root.NodeStatus = NodeStatus.StepFindNode;
                SaveHistory($"Узел со значением Key({data}), не найден!");
            }
            else
            {
                node.NodeStatus = NodeStatus.FindedNode;
                SaveHistory($"Узел со значением Key({data}), найден!");
            }

            return node;
        }

        public void ClearStatusAllNode()
        {
            ClearStatusAllNode(Root);
        }

        public void ApplyHistory(int lastIndex)
        {
            //ClearStatusAllNode();
            if (HistoryCount() < lastIndex || lastIndex < 0) return;

            for (var index = 0; index < lastIndex; index++)
            {
                var threadFindData = History[index];
                Root = threadFindData.DumpedTree;
                //threadFindData.Node.ArrowText = threadFindData.ArrowText;
            }
        }

        public void SaveHistory(string comment)
        {
            History.Add(new HistoryData(Root, comment));
        }

        public HistoryData GetHistory(int index)
        {
            if (index < 0 | index > HistoryCount()) return null;
            return History[index - 1];
        }

        public int HistoryCount()
        {
            return History.Count;
        }

        /// <summary>
        /// Определяет пустое ли дерево
        /// </summary>
        /// <returns>Результат проверки на пустоту</returns>
        public bool IsEmpty()
        {
            return Root == null;
        }

        #endregion

        #region Рекурсивные методы

        /// <summary>
        /// Вычисляет колличество элементов в дереве от узла <paramref name="node"/>
        /// </summary>
        /// <param name="node">Родительский узел с которого начинать считать</param>
        /// <returns>Колличество узлов</returns>
        private int Count(Node node)
        {
            int count = 1;

            if (node.RightChild != null)
                count += Count(node.RightChild);

            if (node.LeftChild != null)
                count += Count(node.LeftChild);

            return count;
        }

        /// <summary>
        /// Вставляет узел с заданным ключом в дерево
        /// </summary>
        /// <param name="data">Ключ</param>
        /// <param name="node">Текущий узел для добавления</param>
        /// <param name="parent">Родительский узел для текущего узла</param>
        private void Insert(int data, Node node, Node parent)
        {
            node.NodeStatus = NodeStatus.FindNode;
            SaveHistory("");

            if (node.Data > data)
            {
                if (node.LeftChild == null)
                {
                    var newNode = new Node
                    {
                        Parent = node,
                        Data = data,
                        NodeStatus = NodeStatus.InsertedNode
                    };

                    node.NodeStatus = NodeStatus.FindedNode;
                    SaveHistory("");

                    node.LeftChild = newNode;
                    SaveHistory("");
                }
                else
                {
                    node.NodeStatus = NodeStatus.StepFindNode;
                    SaveHistory("");

                    Insert(data, node.LeftChild, node);
                }
            }
            else if (node.Data < data)
            {

                if (node.RightChild == null)
                {
                    var newNode = new Node
                    {
                        Parent = node,
                        Data = data,
                        NodeStatus = NodeStatus.InsertedNode
                    };

                    node.NodeStatus = NodeStatus.FindedNode;
                    SaveHistory("");

                    node.RightChild = newNode;
                    SaveHistory("");
                }
                else
                {
                    node.NodeStatus = NodeStatus.StepFindNode;
                    SaveHistory("");

                    Insert(data, node.RightChild, node);
                }
            }
        }

        /// <summary>
        /// Вставляет узел с заданным ключом в дерево
        /// </summary>
        /// <param name="data">Узел для замены узла <paramref name="node"/></param>
        /// <param name="node">Текущий узел</param>
        /// <param name="parent">Родительский узел для текущего узла</param>
        private void Insert(Node data, Node node, Node parent)
        {
            if (node.Data == null || node.Data == data.Data)
            {
                node.Data = data.Data;
                node.LeftChild = data.LeftChild;
                node.RightChild = data.RightChild;
                node.Parent = parent;
                return;
            }

            if (node.Data > data.Data)
            {
                if (node.LeftChild == null) node.LeftChild = new Node();
                Insert(data, node.LeftChild, node);
            }
            else
            {
                if (node.RightChild == null) node.RightChild = new Node();
                Insert(data, node.RightChild, node);
            }
        }

        /// <summary>
        /// Вычисляет глубину дерева от заданного узла <paramref name="root"/>
        /// </summary>
        /// <param name="root">Узел отсчета</param>
        /// <returns>Глубина от узла</returns>
        private int GetDepth(Node root)
        {
            int depth = 0;

            List<Node> level = new List<Node>();
            List<Node> next = new List<Node>();

            level.Add(root);

            int countNode = 1;

            while (countNode != 0)
            {
                countNode = 0;
                foreach (Node tree in level)
                {
                    if (tree == null)
                    {
                        next.Add(null);
                        next.Add(null);
                    }
                    else
                    {
                        next.Add(tree.LeftChild);
                        next.Add(tree.RightChild);

                        if (tree.LeftChild != null) countNode++;
                        if (tree.RightChild != null) countNode++;
                    }
                }

                List<Node> tmp = level;

                depth++;
                level = next;

                next = tmp;
                next.Clear();
            }

            return depth;
        }

        private Node ThreadFind(int data, Node node)
        {
            if (node == null)
                return null;

            node.NodeStatus = NodeStatus.FindNode;
            SaveHistory($"Сравниваем значение теущего узла, с искомым значением key({data})");

            if (node.Data == data)
            {
                node.NodeStatus = NodeStatus.StepFindNode;
                SaveHistory($"Искомое значение Key({data}) == значению Узла({node.Data})");
                return node;
            }

            if (node.Data > data)
            {
                node.NodeStatus = NodeStatus.StepFindNode;
                SaveHistory($"Искомое значение Key({data}) < значения Узла({node.Data}),\nПереходим в левое поддерево");
                return ThreadFind(data, node.LeftChild);
            }

            node.NodeStatus = NodeStatus.StepFindNode;
            SaveHistory($"Искомое значение Key({data}) > значения Узла({node.Data}),\nПереходим в правое поддерево");
            return ThreadFind(data, node.RightChild);
        }

        private void ClearStatusAllNode(Node node)
        {
            if (node == null) return;

            ClearStatusAllNode(node.LeftChild);
            node.ClearStatus();
            node.ArrowText = "";
            ClearStatusAllNode(node.RightChild);
        }

        #endregion

        #region Вывод дерева

        public void DrawTree(ref Canvas gr, double xmin, double ymin)
        {
            if (Root == null) return;

            // Position all of the nodes.
            Root.PositionNode(ref xmin, ymin);

            // Draw all of the links.
            Root.DrawBranches(ref gr);

            // Draw all of the nodes.
            Root.DrawNode(ref gr);
        }

        #endregion

        #region События

        #endregion

        public void Clear()
        {
            Root = null;
            History = new List<HistoryData>();
        }
    }
}