﻿using System.Windows.Media;

namespace Diplom.Logic.Attributes
{
    public class NodeStatusAttribute : System.Attribute
    {
        public Color BorderColor { get; set; }
        public Color BackgroundColor { get; set; }
        public Color ForegroundColor { get; set; }


        public NodeStatusAttribute()
        {
            BorderColor = Colors.Black;
            BackgroundColor = Colors.White;
            ForegroundColor = Colors.Black;
        }

        public NodeStatusAttribute(string borderColor, string backgroundColor, string foregroundColor)
        {
            BorderColor =     (Color) ColorConverter.ConvertFromString(borderColor);
            BackgroundColor = (Color) ColorConverter.ConvertFromString(backgroundColor);
            ForegroundColor = (Color) ColorConverter.ConvertFromString(foregroundColor);
        }

        
    }
   
}
