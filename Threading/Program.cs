﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Threading
{
   class Program
    {
        public static List<string> Lis = new List<string>();

        private static void Main()
        {
            Thread thr = new Thread(Method);
            ManualResetEvent me = new ManualResetEvent(true);


            string command= "";
            while (true)
            {
                Console.Clear();
                foreach (string li in Lis)
                {
                    Console.Write("{0}, ",li);
                }
                Console.WriteLine();
                Console.WriteLine("command: ");
                command = Console.ReadLine();
                if (command == "exit") 
                {
                    break;
                }
                switch (command)
                {
                    case "next": me.Set(); break;
                    case "stop": me.Reset(); break;
                }

            }
            me.Reset();
            me.Close();
            thr.Abort();
            return;
        }

        private static void Method(object state)
        {
            ManualResetEvent MRE = (ManualResetEvent) state;
            MRE.WaitOne();
            Lis.Add("Test");
            MRE.Reset();
            Method(state);
        }
    }
}