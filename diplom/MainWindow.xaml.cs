﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using Diplom.Logic.Common;

namespace Diplom
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Tree Trees { get; set; }
        private InfoWindow _infoWindow;

        private int _stepOfHistory = 1;

        public MainWindow()
        {
            InitializeComponent();

            Trees = new Tree();


            this.LocationChanged += (sender, args) => Update();
            this.SizeChanged += (sender, args) => Update();
            //this.Activated += (sender, args) => Update(); 

            Draw();
        }

        private void Draw()
        {
            this.Activate();
            Mono.Children.Clear();
            Trees.DrawTree(ref Mono, 20, 20);
        }

        private void Update()
        {
            if (_infoWindow != null)
            {
                _infoWindow.Left = this.Left + this.Width;
                _infoWindow.Top = this.Top;
            }
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            _infoWindow?.Close();
        }

      
        private void RandomFillButton_Click(object sender, RoutedEventArgs e)
        {
            _infoWindow?.Close();
            Random r = new Random((int) DateTime.Now.Ticks);

            Trees.Clear();

            List<int> s = new List<int>();

            for (int i = 0; i < 15; i++)
            {
                while (true)
                {
                    var nextI = r.Next(50);
                    if (s.Contains(nextI))
                        continue;

                    s.Add(nextI);

                    Trees.Insert(nextI);
                    break;
                }
            }

            Draw();
        }

        private void ButtonAdd_Click(object sender, RoutedEventArgs e)
        {
            _infoWindow?.Close();
            PromtDialog dialog = new PromtDialog();
            var result = dialog.ShowDialog("Добавление узла", "Введите ключ узла", "Добавить");
            if (result == PromtDialogResult.Ok)
            {
                Trees.Insert(dialog.ResultData);
                MessageBox.Show($"Ключ {dialog.ResultData} успешно добавлен.");
                Draw();
            }
        }

        private void ButtonSearch_Click(object sender, RoutedEventArgs e)
        {
            _infoWindow?.Close();
            PromtDialog dialog = new PromtDialog();
            var result = dialog.ShowDialog("Поиск узла", "Введите ключ узла", "Найти");
            if (result == PromtDialogResult.Ok)
            {
                Trees.Find(dialog.ResultData);
                MessageBox.Show($"Ключ {dialog.ResultData} успешно найден.");
                Trees.ApplyHistory(Trees.HistoryCount());
                Draw();
            }
        }

        private void ButtonRemove_Click(object sender, RoutedEventArgs e)
        {
            _infoWindow?.Close();
            PromtDialog dialog = new PromtDialog();
            var result = dialog.ShowDialog("Удаление узла", "Введите ключ узла", "Удалить");
            if (result == PromtDialogResult.Ok)
            {
                Trees.Remove(Trees.Find(dialog.ResultData));
                MessageBox.Show($"Ключ {dialog.ResultData} успешно удален.");
                Draw();
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            _infoWindow?.Close();
            Trees.Clear();
            Draw();
        }

        private void InitTreeButton_Click(object sender, RoutedEventArgs e)
        {
            _infoWindow?.Close();
            Trees.Clear();
            int[] numbers = new[]
            {
                50, 30, 70, 20, 40, 60, 80, 15, 25, 35, 45, 55, 65, 75, 85, 12, 18, 22, 28, 32, 38, 42, 48, 52, 58, 62,
                68, 72, 78, 82, 88, 11, 13, 17, 19, 21, 23, 27, 29, 31, 33, 37, 39, 41, 43, 47, 49, 51, 53, 57, 59, 61,
                63, 67, 69, 71, 73, 77, 79, 81, 83, 87, 89
            };
            numbers = new[]
            {
                5,7,3,2,4,6,8
            };

            for (int i = 0; i < numbers.Length; i++)
            {
                Trees.Insert(numbers[i]);
            }
            Draw();
        }

        private void StepSearchButton_Click(object sender, RoutedEventArgs e)
        {
            _infoWindow?.Close();
            PromtDialog dialog = new PromtDialog();
            var result = dialog.ShowDialog("Поиск узла", "Введите ключ узла", "Найти");
            if (result != PromtDialogResult.Ok) return;

            this.Activate();

            Trees.Find(dialog.ResultData);
            if (Trees.HistoryCount() <= 0) return;
            Trees.ApplyHistory(1);
            Draw();

            var trees = Trees;
            _infoWindow = new InfoWindow(ref trees);

            _infoWindow.ButtonClickEvent += (o, args) =>
            {
                Mono.Focus();
                Draw();
            };
            _infoWindow.Show();
            Update();
        }

        private void StepAddButton_Click(object sender, RoutedEventArgs e)
        {
            _infoWindow?.Close();
            PromtDialog dialog = new PromtDialog();
            var result = dialog.ShowDialog("Добавление узла", "Введите ключ узла", "Добавить");
            if (result != PromtDialogResult.Ok) return;
            this.Activate();

            Trees.Insert(dialog.ResultData);

            if (Trees.HistoryCount() <= 0) return;

            Trees.ApplyHistory(1);
            Draw();

            var trees = Trees;
            _infoWindow = new InfoWindow(ref trees);

            _infoWindow.ButtonClickEvent += (o, args) =>
            {
                Mono.Focus();
                Draw();
            };
            _infoWindow.Show();

            Draw();
        }

        private void StepRemoveButton_Click(object sender, RoutedEventArgs e)
        {
            _infoWindow?.Close();
            PromtDialog dialog = new PromtDialog();
            var result = dialog.ShowDialog("Удаление узла", "Введите ключ узла", "Удалить");
            if (result != PromtDialogResult.Ok) return;
            this.Activate();

            Trees.Remove(Trees.Find(dialog.ResultData));

            if (Trees.HistoryCount() <= 0) return;

            Trees.ApplyHistory(1);
            Draw();

            var trees = Trees;
            _infoWindow = new InfoWindow(ref trees);

            _infoWindow.ButtonClickEvent += (o, args) =>
            {
                Mono.Focus();
                Draw();
            };
            _infoWindow.Show();

            Draw();
        }

        private void TestRemoveButton_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Не реализованно");
            Draw();
        }

        private void TestAddButton_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Не реализованно");
            Draw();
        }

        private void TestSearchButton_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Не реализованно");
            Draw();
        }
    }
}