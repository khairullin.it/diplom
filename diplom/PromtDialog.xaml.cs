﻿using System;
using System.Runtime.InteropServices;
using System.Security;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Interop;
using System.Windows.Threading;

namespace Diplom
{
    public enum PromtDialogResult
    {
        Ok,
        Cancel,
    }

    public partial class PromtDialog : Window
    {
        private PromtDialogResult _resultAction = PromtDialogResult.Cancel;
        public int ResultData { get; set; } = -1;

        [SuppressUnmanagedCodeSecurity, SecurityCritical,
         DllImport("user32.dll", EntryPoint = "EnableWindow", CharSet = CharSet.Auto)]
        public static extern bool IntEnableWindowNoThrow(HandleRef hWnd, bool enable);

        [DllImport("user32")]
        internal static extern bool EnableWindow(IntPtr hwnd, bool bEnable);

        public void ShowModal()
        {
            MainWindow window = (MainWindow) Application.Current.MainWindow;
            if (window == null) return;

            IntPtr handle = (new WindowInteropHelper(window)).Handle;
            EnableWindow(handle, false);

            DispatcherFrame frame = new DispatcherFrame();

            Closed += delegate
            {
                EnableWindow(handle, true);
                frame.Continue = false;
            };

            Show();
            Dispatcher.PushFrame(frame);
        }

        

        public PromtDialog()
        {
            InitializeComponent();
        }

        public int GetResult()
        {
            return ResultData;
        }

        public PromtDialogResult ShowDialog(string caption, string label, string buttonOkText = "Ok")
        {
            LabelText.Text = label;
            Title = caption;
            ButtonOk.Content = buttonOkText;

            ShowModal();

            return _resultAction;
        }

        private void ButtonOk_Click(object sender, RoutedEventArgs e)
        {
            string s = ValueBox.Text;
            if (int.TryParse(s,out var res))
            {
                ResultData = res;
            }
            else
            {
                MessageBox.Show("Не введен текст, возникла ошибка преобразования.", "Предупреждение");
                return;
            }

            _resultAction = PromtDialogResult.Ok;
            Close();

        }

        private void ButtonCancel_Click(object sender, RoutedEventArgs e)
        {
            _resultAction = PromtDialogResult.Cancel;
            Close();

        }

        private void TextBoxPasting(object sender, DataObjectPastingEventArgs e)
        {
            if (e.DataObject.GetDataPresent(typeof(string)))
            {
                var text = (string) e.DataObject.GetData(typeof(string));
                if (!IsTextAllowed(text))
                {
                    e.CancelCommand();
                }
            }
            else
            {
                e.CancelCommand();
            }
        }

        private static bool IsTextAllowed(string text)
        {
            return !Regex.IsMatch(text, "[^0-9.-]+");
        }
    }
}
